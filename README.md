# Biased gene cluster detection:

This script allows to detect genomic regions enriched for biased genes.

## Usage

```
./DE_position.py -h
usage: DE_position.py [-h] -b B -e E -o O -c C -boot BOOT [-p P] [-step STEP]
                      [-window WINDOW]

Look for biased genes regions

optional arguments:
  -h, --help      show this help message and exit
  -b B            Bedfile of the transcripts, mandatory
  -e E            Expression data, mandatory
  -o O            Bootstraps output file prefix, mandatory
  -c C            Chromosome sizes, mandatory
  -boot BOOT      Bootstrap number
  -shrink SHRINK  lfcShrink from DEseq used or not
  -p P            padj treshold (default: 0.05)
  -step STEP      Step size (default: 100000)
  -window WINDOW  Window size (default: 2000000)
```

## Python libraries needed

Versions with which the script was developed:
 - `python` version 3.5.2
 - `argparse` version 1.1
 - `pandas` version 0.24.2

## Inputs

### Mandatory

 - *Bed file of the genes: (exemple)*

```
head genes.bed

chr1    3569    5689    g1
chr1    3569    5785    g1
chr1    10566   19568   g2
chr2    14555   24789   g3
chr2    56689   60532   g25
chr3    4578    8652    g65
```

Each gene can have multiple transcripts, but need to have the same gene identifier. They will be merged during the treatment.

 - *Expression data: (exemple)*
 
```
head expression.csv

baseMean    log2FoldChange  padj
g18957	3624.18966889584    3.77561886127686	1.11919448211977e-33
g3498	1048.90265022281    -2.69239517225126	4.86126460751224e-33
g9621	1340.5387144472	-0.979770120099396	4.86126460751224e-33
g19753	1800.1627523857	-5.02347041362929	2.64926479631494e-20
g14997	1605.56095338252	1.80903549047542	3.50038551336759e-17
```
 
A tabulation separated table. 4 columns, gene identifier, base mean normalized DESeq count, log2FC, adjusted pvalue. There are only 3 headers, so that genes are considered as row names.
 
 - *Output file prefix: (exemple)*
  
The file prefix where you want to write the output. You can give the full path. Exemple: `/home/jean-didier/Gene_clusters/My_species/Run_2019_07`

 - *Chromosome size file : (exemple)*

```
head chromosome.csv

chromosome      length  order
chr1      65320744        1
chr2 92932883        2
chr3      54210254        3
chr4      64166213        4
```

Chromosome length file. 3 columns, tabulation separated, chromosome name, length, chromosome number. Need to keep this header.

 - *Bootstrap number:*

The bootstrap number you want to use.

Duration estimation : 1min / 10k bootstraps / chromosome.

### Optional parameters

 - *LFC shrink used:*
 
If LFC shrink was used in the LFC calculation, please use -shrink. If you didn't, all LFC with padj < threshold choosen will be set to 0.

 - *Padj threshold : default = `0.05`*

The analysis will only consider genes with padj below this treshold.

 - *Step size : default = `100 000` nt.*

The analysis uses a sliding window. This is the step of the window. In each iteration the window shifts of the step size.

 - *Window size : default = `2 000 000`nt.*

The size of the window.

Exemple : 
```
Step : 1 -                                  Step : 5 -
Window : 20 -                               Window : 20 -

Iteration 0: --------------------           Iteration 0: --------------------   
Iteration 1:  --------------------          Iteration 1:      --------------------
Iteration 2:   --------------------         Iteration 2:           --------------------
Iteration 3:    --------------------        Iteration 3:                --------------------
Etc ...
```

## Output

Observed profile, before bootstrap calculation:

```
head Run_2019_07chr1

chromosome  bin meanFold	geneNum
chr1        0   0.1568		65
chr1        1   0.2259		59
chr1        2   0.3544		42
```

This file gives the mean log2 fold change of the genes in each window (column bin). This is generated before bootstrap calculation.

After bootstrap calculation :

```
head Run_2019_07_Boot_chr1

chromosome  bin meanFold    geneNum	Bootstrap_male  Bootstrap_female
chr1        0   0.1568      65		9500            500
chr1        1   0.2259      59		9100            900    
chr1        2   0.3544      42		9600            400
```

The bootstrap value tells on how many bootstrap replicates, the observed fold change is higher (for male) or lower (for female).

It was developed to work with sex biased genes, thats why male and female are output. If you work with other conditions, male correspond to positive log2FC, and female to negative log2FC.

Regions with very high or low bootstraps are considered enriched in biased genes.
