#!/usr/bin/env python3

Usage="""
Look for biased genes regions
"""
# Packages:
try :
        import argparse
except ImportError as err :
        print ("Unable to find argparse, sorry : {}. Exiting...".format(err))
        exit(1)
try :
        import pandas as pd
except ImportError as err :
        print ("Unable to find pandas, sorry : {}. Exiting...".format(err))
        exit(1)
try :
        import random as rd
except ImportError as err :
        print ("Unable to find random, sorry : {}. Exiting...".format(err))
        exit(1)

from timeit import default_timer as timer
from datetime import timedelta


             
parser = argparse.ArgumentParser(description=Usage)

parser.add_argument("-b",help="Bedfile of the transcripts, mandatory",required=True)
parser.add_argument("-e",help="Expression data, mandatory",required=True)
parser.add_argument("-o",help="Bootstraps output file  prefix, mandatory",required=True)
parser.add_argument("-c",help="Chromosome sizes, mandatory", required=True)
parser.add_argument("-boot",help="Bootstrap number",required=True,type=int)
parser.add_argument('-shrink', dest='shrink', action='store_true',help="lfcShrink from DEseq used")
parser.add_argument("-p",required=False,default=0.05,type=int,help = "padj treshold (default: %(default)s)")
parser.add_argument("-step",required=False,default=100000,type=int,help = "Step size (default: %(default)s)")
parser.add_argument("-window",required=False,default=2000000,type=int,help = "Window size (default: %(default)s)")
parser.set_defaults(shrink=False)
args=parser.parse_args()

# Check window and step size
if (args.step > args.window):
	print("Error, Step larger than window ... Exiting.")
	exit(1)
if (args.window%args.step != 0):
	print("Error, widow size need to be multiple of step size... Exiting.")
	exit(1)



# Set fold change to 0 when padj is too high.
exp = pd.read_csv(args.e,sep="\t",header=0)   					# //Read the expression table
exp['gene'] = exp.index
exp.loc[exp['padj'].isna(),'log2FoldChange']=0					# Set de the fold change to 0 when neeeded (if LFCshrink was not used)
if not args.shrink :
	exp.loc[exp['padj']>=args.p,'log2FoldChange']=0

# Read the bed and merge transcripts.
bed = pd.read_csv(args.b,sep="\t",header=None)
bed.columns=['chr','start','end','gene']
bed = bed.groupby(['gene','chr']).agg({'start':'min','end':'max'}).reset_index() 		# If multiple transcripts, take max and min.
bed['position'] = bed.apply(lambda row: int(round((row.start+row.end)/2,0)), axis=1)	# Get position center

# Read chromosome size.
chrsize = pd.read_csv(args.c,sep="\t",header=0) # Open
Bins = {}
bed['bin'] = int()
bed=bed.loc[bed['chr'].isin(chrsize['chromosome'])] # Filter BED file to keep interect chromosomes.

for chr in chrsize['chromosome']:				
	Bins[chr]=range(0,int(chrsize.loc[chrsize['chromosome']==chr,'length'])+args.step,args.step)									# Create the bins, step = step
	df=bed.loc[bed['chr']==chr]
	binchr = pd.cut(df['position'].tolist(), bins=Bins[chr], labels=range(0,len(Bins[chr])-1,1), include_lowest=False).tolist()		# Associate position to bin
	bed.loc[bed['chr']==chr,'bin'] = binchr																							
	bed['bin'] = bed['bin'].astype(int)									# Convert in integer

# Merge expression an localisation :
merged = bed.merge(exp,left_on='gene',right_on='gene',how='left') # Keep all genes from bed to add not expressed

del merged['start']
del merged['end']
del merged['padj']
merged = merged.fillna(0)

# Cut baseMean
## binBase = [0,10,100,1000,10000,100000,1000000,10000000,100000000]
binBase=[0]
## baseCut = pd.cut(merged['baseMean'].tolist(), bins=binBase, labels=binBase[0:8], include_lowest=True).tolist()		# Associate position to bin, include lowest in the bin
##merged['baseMean_bin'] = baseCut
merged['baseMean_bin'] = binBase*len(merged['baseMean'].tolist()) ## If no classes
merged['baseMean_bin'] = merged['baseMean_bin'].astype(int)
#merged.to_csv(args.o+"FCBed",index=False,sep="\t",encoding="utf-8")
#exit(1)
#print("stop!")
# Window size in bin :
winsize = args.window//args.step

def Merged_to_draw(winsize,merged,stepsize): 
	"""
	Function to go from merged table to ready to draw table. Return pandas dataframe.
	"""
	
	perBinDict = {'chromosome':[], 'bin':[], 'meanFold':[], 'geneNum':[]}
	BinFold = merged['log2FoldChange'].tolist()
	#for chr in chrsize['chromosome']:
		#binmax = chrsize.loc[chrsize['chromosome']==chr,'length'].tolist()[0]//stepsize			# Bin number in the chromosome
		#merged_chr = merged.loc[merged['chr']==chr]
		#merged_chr = merged_chr.sort_values('bin')
		
	list_bins=merged['bin'].tolist()
	gene_rank_start = 0
	gene_rank_end = 0
	
	for step in range(0,binmax+1):
		
		  																	# for each bin
		if (winsize%2==0):																		# Get the bin interval
			binlow = int(step-(winsize/2)+1)
			binhigh = int(step+(winsize/2))
		else:
			binlow = int(step - int(winsize/2))
			binhigh = int(step + int(winsize/2))
		fins=0
		while not (binlow <= list_bins[gene_rank_start]): 										# Then if low bin intervalsmaller than what Iwant to get :
			if gene_rank_start < len(list_bins):												# increase of one 
				gene_rank_start+=1
			if gene_rank_start == len(list_bins):												# if we reach the end
				gene_rank_start = len(list_bins)-1
				fins = 1
				break
			
		stop=False
		fin=0
		while (list_bins[gene_rank_end] <= binhigh):
			if gene_rank_end < len(list_bins):
				gene_rank_end+=1
				
			if gene_rank_end == len(list_bins) :
				gene_rank_end = len(list_bins)-1
				fin = 1
				break
		
		
		#subBin = merged.iloc[range(gene_rank_start+fins, gene_rank_end+fin),:]	
		subBin = BinFold[gene_rank_start+fins:gene_rank_end+fin]
		
		#binMean=subBin['log2FoldChange'].mean()
		#binMean=s.mean(subBin)
		try:
			binMean=sum(subBin) / float(len(subBin))
		except ZeroDivisionError:
			binMean=0
		
		#perBin.iloc[row] = [chr,step,binMean]
		perBinDict['chromosome'].append(chr)
		perBinDict['bin'].append(step)
		perBinDict['meanFold'].append(binMean)
		perBinDict['geneNum'].append(len(subBin))
		
		
		
	#perBin = pd.DataFrame(perBinDict,columns=['chromosome','bin','meanFold'])
	
	#print (timedelta(seconds=end-start))
	return(perBinDict)

pd.options.mode.chained_assignment = None

# Split the table for the shuffle


# For each chromosome :
for chr in chrsize['chromosome']:
	# Bin number in this chr
	binmax = chrsize.loc[chrsize['chromosome']==chr,'length'].tolist()[0]//args.step			# Bin number in the chromosome
	
	# Get merged for this chr
	merged_chr = merged.loc[merged['chr']==chr]
	# Sort by gene order in term of bin.
	merged_chr = merged_chr.sort_values('bin')
	
	# Generate the observed table :
	perBinDict = Merged_to_draw(winsize,merged_chr,args.step)
	# Save as CSV
	perBin = pd.DataFrame(perBinDict,columns=['chromosome','bin','meanFold','geneNum'])
	perBin.to_csv(args.o+chr,index=False,sep="\t",encoding="utf-8")
	
	# Initialize boostraps :
	perBin['Bootstrap_male']=0
	perBin['Bootstrap_female']=0
	
	## Create a dict of folds list per binbase :
	folds_perbase = {}
	# Create a dict of nbfolds per binbase :
	nbfolds_perbase={}
	
	for base in binBase:
		subMerged = merged.loc[merged['baseMean_bin']==base]					# Select the base to shuffle
			
		Folds = subMerged['log2FoldChange'].tolist()
		folds_perbase[base] = Folds							# Retrieve FOlds
		
		# Dict base : nbfold
		subMerged_chr = merged_chr.loc[merged_chr['baseMean_bin']==base] # Les bon basebin du chr
		Folds = len(subMerged_chr['log2FoldChange'].tolist()) # Les folds associés sur ce chromosome
		nbfolds_perbase[base]=Folds
	
	# Start bootstraps:	
	for boot in range(0,args.boot):
		if (boot % 10000 == 0):
			now=timer()
			try :
				print(boot, timedelta(seconds=now-before))
			except :
				pass
			before=timer()
		#start=timer()
		
		
		try :
			del remodeled
		except :
			pass
		
		
		# Dict containing the folds in each base :
		randFolds={}
		for base in binBase:
			
			
			Folds = rd.sample(folds_perbase[base],nbfolds_perbase[base])	# Sample autant sur tout le génome
			randFolds[base]=Folds												
			#subMerged_chr['log2FoldChange'] = Folds	# Remplace
		
		# Remodel the table										
		remodeled=merged_chr
		remodeled = remodeled.sort_values('baseMean')
		
		# Sort dict, get folds and add them to merged in the good order.
		b=list(randFolds.items())	
		randFoldssort = sorted(b, key=lambda b: int(b[0]))
		listFolds = [i[1] for i in randFoldssort]
		listFoldsR = [item for sublist in listFolds for item in sublist]
		remodeled['log2FoldChange']	= listFoldsR
		
		# sort by bin before going in he draw function
		remodeled = remodeled.sort_values('bin')
		perBinRem = Merged_to_draw(winsize,remodeled,args.step)
		
		# Calcul bootstraps
		ObsFolds = perBinDict['meanFold']
		BootFold = perBinRem['meanFold']

		ActualBoot = [int(x >y) for x, y in zip(ObsFolds, BootFold)]
		PreviousSumM = perBin['Bootstrap_male'].tolist()
		perBin['Bootstrap_male'] = [int(x +y) for x, y in zip(PreviousSumM, ActualBoot)]
		
		ActualBoot = [int(x <y) for x, y in zip(ObsFolds, BootFold)]
		PreviousSumF = perBin['Bootstrap_female'].tolist()
		perBin['Bootstrap_female'] = [int(x +y) for x, y in zip(PreviousSumF, ActualBoot)]
		#end=timer()
		#print(timedelta(seconds=end-start))

	perBin.to_csv(args.o+"_Boot_"+chr,index=False,sep="\t",encoding="utf-8")

